﻿using UnityEngine;
using System.Collections;

public class FantasyController : MonoBehaviour {

	public Animator FantasyAnimator; 

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.UpArrow)) {
			FantasyAnimator.SetFloat ("SpeedF", 1);
		} else {
			FantasyAnimator.SetFloat ("SpeedF", 0);
		}
		if (Input.GetKey (KeyCode.LeftShift)) {
			FantasyAnimator.SetBool ("RunF", true);
		} else {
			FantasyAnimator.SetBool ("RunF", false);
		} if (Input.GetKey (KeyCode.A)) {
			FantasyAnimator.SetTrigger ("AttackF");
		} if (Input.GetKeyDown (KeyCode.Q)) {
			FantasyAnimator.SetTrigger ("SkillF");
		}
	
	}
}
