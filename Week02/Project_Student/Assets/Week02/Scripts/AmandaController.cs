﻿using UnityEngine;
using System.Collections;

public class AmandaController : MonoBehaviour {

	public Animator AmandaAnimator;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.UpArrow)) {
			AmandaAnimator.SetFloat ("SpeedAM", 1);
		} else {
			AmandaAnimator.SetFloat ("SpeedAM", 0);
		}
		if (Input.GetKey (KeyCode.LeftShift)) {
			AmandaAnimator.SetBool ("Amadarun", true);
		} else {
			AmandaAnimator.SetBool ("Amadarun", false);
		} if (Input.GetKey (KeyCode.A)) {
			AmandaAnimator.SetTrigger ("Amadaroll");
		} if (Input.GetKeyDown (KeyCode.Q)) {
			AmandaAnimator.SetTrigger ("Amandafly");
		}
	}
}
