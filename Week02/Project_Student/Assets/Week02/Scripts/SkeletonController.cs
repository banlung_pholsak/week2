﻿using UnityEngine;
using System.Collections;

namespace IGD246.Week02 {

    public class SkeletonController : MonoBehaviour {

		public Animator skelortonAnimator;
		public int MyHP;

        // Use this for initialization
        void Start() {
			MyHP = 100;
        }

        // Update is called once per frame
        void Update() {
			if (Input.GetKey (KeyCode.UpArrow)) {
				skelortonAnimator.SetFloat ("Speed", 1);
			} else {
				skelortonAnimator.SetFloat ("Speed", 0);
			}
			if (Input.GetKey (KeyCode.LeftShift)) {
				skelortonAnimator.SetBool ("IsSprint", true);
			} else {
				skelortonAnimator.SetBool ("IsSprint", false);
			}
			if (Input.GetKeyDown (KeyCode.Space)) {
				MyHP = 0;
			}
			skelortonAnimator.SetInteger ("HP", MyHP);

			//if (Input.GetKeyDown (KeyCode.D)) {
			//	skelortonAnimator.SetBool ("Attack", true);
			//}
			//
			//if (Input.GetKeyUp (KeyCode.D)) {
			//	skelortonAnimator.SetBool ("Attack", false);
			//}
			if (Input.GetKeyDown (KeyCode.D)) {
				skelortonAnimator.SetTrigger ("AttackTrigger");
			}
		} 
	}


}